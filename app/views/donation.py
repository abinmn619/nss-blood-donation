from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from app.permissions import IsAllowedToModify

from app.models import Donation
from app.serializers import DonationSerializer

class DonationViewset(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsAllowedToModify,)
    serializer_class = DonationSerializer
    
    def get_queryset(self):
        college = self.request.user.volunteerprofile.college
        return Donation.objects.filter(student__college=college)